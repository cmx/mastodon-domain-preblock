# mastodon-domain-prelock

Allows Mastodon users to pre-block domains.

License: MIT
