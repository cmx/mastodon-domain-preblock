<?php
session_start();

if (!isset($_SESSION["access_token"])) {
    echo "您尚未登录。";
    exit;
}

$accessToken = $_SESSION["access_token"];
$options = [
    'http' => [
        'header' => "Authorization: Bearer {$accessToken}\r\n",
        'method' => 'GET',
    ],
];
$context = stream_context_create($options);
$result = file_get_contents("https://m.cmx.im/api/v1/accounts/verify_credentials", false, $context);

if ($result === FALSE) {
    echo "获取帐户详情时发生错误。";
    exit;
}

$userData = json_decode($result, true);
echo "您已登录为： " . $userData["username"];

$context = stream_context_create($options);
$blockedDomains = file_get_contents("https://m.cmx.im/api/v1/domain_blocks", false, $context);

$blockedDomains = json_decode($blockedDomains, true);

?>

<html>
<head>
    <title>帐号管理</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
</head>
<body class="container">

<a href="logout.php" class="btn btn-link">退出登录</a>

<h2>已屏蔽的域名</h2>

<a href="https://m.cmx.im/domain_blocks" target="_blank" class="btn btn-link">管理已屏蔽的域名</a>

<table class="table">
    <thead class="thead-dark">
    <tr>
        <th>域名</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($blockedDomains as $domain) {
        echo "<tr><td>{$domain}</td></tr>";
    }
    ?>
    </tbody>
</table>
<h2>添加新的屏蔽域名</h2>
<form method="POST" action="block_domain.php">
    <div class="mb-3">
        <input type="text" name="domain" class="form-control" placeholder="要屏蔽的域名" value="threads.net" required>
    </div>
    <button type="submit" class="btn btn-primary">添加</button>
</form>

</body>
</html>
