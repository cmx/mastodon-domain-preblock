<?php
session_start();

if (!isset($_SESSION["access_token"]) || empty($_POST["domain"])) {
    header("Location: login.php");
    exit;
}

$accessToken = $_SESSION["access_token"];
$domainToBlock = $_POST["domain"];

// Prepare the headers
$headers = [
    "Authorization: Bearer {$accessToken}"
];

// Prepare the form data
$data = [
    "domain" => $domainToBlock
];

// Create a cURL handle
$ch = curl_init();

// Set the URL
curl_setopt($ch, CURLOPT_URL, "https://m.cmx.im/api/v1/domain_blocks");

// Set the HTTP method to POST
curl_setopt($ch, CURLOPT_POST, true);

// Set the headers
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

// Set the form data
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

// Set option to return the result as a string
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute the cURL request
$response = curl_exec($ch);

// Close the cURL handle
curl_close($ch);

// Decode the response
$responseData = json_decode($response, true);

// Check for errors
if (isset($responseData["error"])) {
    echo "Error occurred: " . $responseData["error"];
    exit;
}

// If no error occurred, redirect back to the main page
header("Location: profile.php");
exit;
?>
