<?php
$redirectUri = "https://my.cmx.im/mastodon-login/callback.php";
$scope = "read:accounts+read:blocks+write:blocks";
$state = rand();

$url = "https://m.cmx.im/oauth/authorize?response_type=code&client_id=".CLIENT_ID."&redirect_uri={$redirectUri}&scope={$scope}&state={$state}";

header("Location: {$url}");
exit;
?>
