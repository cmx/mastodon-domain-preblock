<?php
$code = $_GET["code"];
$redirectUri = "https://my.cmx.im/mastodon-login/callback.php";

$data = [
    'grant_type' => 'authorization_code',
    'code' => $code,
    'client_id' => CLIENT_ID,
    'client_secret' => CLIENT_SECRET,
    'redirect_uri' => $redirectUri,
];

$options = [
    'http' => [
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data),
    ],
];
$context  = stream_context_create($options);
$result = file_get_contents("https://m.cmx.im/oauth/token", false, $context);

if ($result === FALSE) {
    echo "Error occurred while obtaining access token.";
    exit;
}

$responseData = json_decode($result, true);
$accessToken = $responseData["access_token"];

session_start();
$_SESSION["access_token"] = $accessToken;

header("Location: profile.php");
exit;
?>
